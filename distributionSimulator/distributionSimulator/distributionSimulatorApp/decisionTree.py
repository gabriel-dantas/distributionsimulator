'''
Created on 15 de ago de 2018

@author: gabriel
'''

import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from .models import CaracteristicaRecurso
from .models import Recurso
from .models import CaracteristicaNodeCGM
from sklearn.tree import export_graphviz
import operator
from scipy.linalg._solve_toeplitz import float64
from matplotlib import pyplot as plt

passei = False

def showTree(regressor, recursosPontuados):
    export_graphviz(regressor, out_file='tree.dot', feature_names=['pontuacao'], filled=True,node_ids=True,class_names=['pontuacao', 'recurso'])
    '''plt.figure(figsize=(8,6))
    plt.plot(recursosPontuados['pontuacao'], recursosPontuados['recurso'], 'o')
    plt.xlabel('recurso')
    plt.ylabel('pontuacao')
    plt.show()
 '''
 
def createTempFile(recursosPontuados):
    dataframe = pd.DataFrame(list(recursosPontuados.items()), columns=['recurso', 'pontuacao'])
    return dataframe

def buildRegressionTree(recursosPontuados):
    recursosTeste = createTempFile(recursosPontuados)
    sizeArray = np.prod(recursosTeste.shape)
    regressor = DecisionTreeRegressor(max_depth=sizeArray)
    regressor.fit(np.array([recursosTeste['pontuacao']]).T, recursosTeste['recurso'])
    global passei
    if passei == False:
        showTree(regressor, recursosTeste)
        passei = True
    
    return regressor

def predictLearningProcess(regressor,tarefaCaracteristica):
    return regressor.predict(tarefaCaracteristica.nivel_complexidade)
 
def prepareDictToTree(idTarefa):
    
    caracteristicaTarefa = CaracteristicaNodeCGM.objects.pesquisarCaracteristicaNodeTarefaPorTarefa(idTarefa)
    idCaracteristicas = []
    multiplier = 0

    for caracteristica in caracteristicaTarefa:
        idC = None
        idC = caracteristica.id_caracteristica.id_caracteristica
        multiplier = multiplier + caracteristica.nivel_complexidade
        idCaracteristicas.append(idC)
    
    if multiplier > 0:
        multiplier = multiplier * 10
    
    recursos = Recurso.objects.all()
    recursoPontuacao = {}
    for recurso in recursos:
        caracteristicasRecurso = CaracteristicaRecurso.objects.pesquisarPorRecurso(recurso.id_recurso)
        pontuacao = 0
        for caracteristicaRecurso in caracteristicasRecurso:
            
            if caracteristicaRecurso.id_caracteristica.id_caracteristica in idCaracteristicas:
                pontuacao = pontuacao + (caracteristicaRecurso.nivel_conhecimento * multiplier)
            
            else:
                pontuacao = pontuacao + caracteristicaRecurso.nivel_conhecimento
        
        recursoPontuacao[recurso.id_recurso] = pontuacao
        
    return recursoPontuacao

def machineLearningApply(tipo, recursoPontuacao):
         
    #Segundo passo aplicar arvore de regressao
    
    regressor = buildRegressionTree(recursoPontuacao)
    
    #Terceiro passo comecar as predicoes
    
    result = None
    if tipo == 'melhor':
        t = max(recursoPontuacao.items(), key=operator.itemgetter(1))[0]
        predictionFactor = recursoPontuacao[t] * 10
        result = regressor.predict(float64(predictionFactor))
        result = result.tolist()
        if result[0].is_integer():
            index = int(result[0])
            if index not in recursoPontuacao:
                result[0] = t
            else:
                if index != t:
                    result[0] = t
        else:
            result[0] = t
    else:
        t = min(recursoPontuacao.items(), key=operator.itemgetter(1))[0]
        result = regressor.predict(0.)
        result = result.tolist()
        if result[0].is_integer():
            index = int(result[0])
            if index not in recursoPontuacao:
                result[0] = t
            else:
                if index != t:
                    result[0] = t
        else:
            result[0] = t  
        
    return result

def learningSimulation(qtdTime, idTarefa):
    
    qtdTime = int(qtdTime)
    primeiraPassada = True
    time = []
    
    escolhidosComPontuacao = {}
    
    while qtdTime > 0:
       
        if primeiraPassada:
            recursoPontuacao = prepareDictToTree(idTarefa)
            primeiroRecurso = machineLearningApply('melhor', recursoPontuacao)
            time.append(int(primeiroRecurso[0]))
            escolhidosComPontuacao[int(primeiroRecurso[0])] = recursoPontuacao[primeiroRecurso[0]]
            del recursoPontuacao[int(primeiroRecurso[0])]
            primeiraPassada = False
        else:
            outroRecurso = machineLearningApply('pior', recursoPontuacao)
            time.append(int(outroRecurso[0]))
            escolhidosComPontuacao[int(outroRecurso[0])] = recursoPontuacao[outroRecurso[0]]
            del recursoPontuacao[int(outroRecurso[0])]
        
        qtdTime = qtdTime - 1
        
    global passei
    passei = False
    return time,escolhidosComPontuacao


def timeSimulation(qtdTime, idTarefa):
    
    qtdTime = int(qtdTime)
    time = []
    
    escolhidosComPontuacao = {}
    
    recursoPontuacao = prepareDictToTree(idTarefa)
    
    while qtdTime > 0:
       
        primeiroRecurso = machineLearningApply('melhor', recursoPontuacao)
        time.append(int(primeiroRecurso[0]))
        escolhidosComPontuacao[int(primeiroRecurso[0])] = recursoPontuacao[int(primeiroRecurso[0])]
        del recursoPontuacao[int(primeiroRecurso[0])]
        qtdTime = qtdTime - 1
        
    global passei
    passei = False
    return time,escolhidosComPontuacao