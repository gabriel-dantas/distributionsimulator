'''
Created on 28 de out de 2017

@author: gabriel
'''
from .models import Tarefa
from .models import CaracteristicaTarefa
from .models import Recurso
from .models import CaracteristicaRecurso
from .models import Caracteristica
from .dto import SimuladorTimeEscolhidos
from .dto import RecursoPontuado

class Simulador():
    def simuladorAprendizado(self,numeroTime,id_tarefa):
        tarefa = Tarefa.objects.pesquisaPorId(id_tarefa).first()
        caracteristicasTarefa = CaracteristicaTarefa.objects.pesquisarPorTarefa(tarefa).values_list('id_caracteristica', 'nivel_complexidade')
        listaCaracteristicaTarefa = []
        
        for caracteristicaTarefa in caracteristicasTarefa:
            ct = CaracteristicaTarefa()
            caracteristica = Caracteristica.objects.pesquisaPorId(caracteristicaTarefa[0]).first()
            ct.id_caracteristica = caracteristica
            ct.nivel_complexidade = caracteristicaTarefa[1]
            listaCaracteristicaTarefa.append(ct)
        
        recursos = Recurso.objects.all()
        
        recursosPontuados = []
        
        for recurso in recursos:
            recursoPontuado = RecursoPontuado()
            recursoPontuado.recurso = recurso
            recursoPontuado.pontuacao = 0
            caracteristicasRecurso = CaracteristicaRecurso.objects.pesquisarPorRecurso(recurso).values_list('id_caracteristica', 'nivel_conhecimento')
            for caracRec in caracteristicasRecurso:
                caracContidaNaTarefa = False
                for caracTask in listaCaracteristicaTarefa:
                    if caracTask.id_caracteristica.id_caracteristica == caracRec[0]:
                        caracContidaNaTarefa = True
                
                if caracContidaNaTarefa:
                    recursoPontuado.pontuacao = recursoPontuado.pontuacao + (caracRec[1] * 3)
                else:
                    recursoPontuado.pontuacao = recursoPontuado.pontuacao + caracRec[1]
                    
            recursosPontuados.append(recursoPontuado)
        
        recursosPontuados.sort(key=RecursoPontuado.getKeyRecursosPontuados, reverse=True)
    
        simuladorTimeEscolhido = []
        for rp in recursosPontuados:
            parteTime = SimuladorTimeEscolhidos()
            if simuladorTimeEscolhido == []:
                parteTime.ordem_escolha = 1
                parteTime.recurso = rp.recurso
                simuladorTimeEscolhido.append(parteTime)
            elif len(simuladorTimeEscolhido) < int(numeroTime) - 1:
                parteTime.ordem_escolha = len(simuladorTimeEscolhido) + 1
                parteTime.recurso = rp.recurso
                simuladorTimeEscolhido.append(parteTime)
            elif len(simuladorTimeEscolhido) == int(numeroTime) - 1:
                parteTime.ordem_escolha = numeroTime
                parteTime.recurso = recursosPontuados[len(recursosPontuados) - 1].recurso
                simuladorTimeEscolhido.append(parteTime)
            
        return simuladorTimeEscolhido
    
    