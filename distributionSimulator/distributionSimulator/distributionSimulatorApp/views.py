
from datetime import datetime
from django.shortcuts import render
from django.contrib import messages
from .forms import CaracteristicaForm
from .models import Caracteristica
from .forms import RecursoForm
from .models import Recurso
from .models import Tarefa
from .dto import CaracteristicaRecursoDto
from .forms import TarefaForm
from .dto import SimuladorRecursoDto
from .dto import SimuladorTarefaDto
from .models import CaracteristicaRecurso
from .models import CaracteristicaTarefa
from .forms import SimuladorForm
from .forms import DiagramaForm
from .forms import Tarefa2Form
from .models import NodeCGM
from .dto import CaracteristicaTarefaDto
from .forms import CaracteristicaTarefa2Form
from django.shortcuts import redirect
from .models import Time
from .forms import FeedBackForm
from django.forms.forms import Form
from django.template.context_processors import request


def index(request):
    return render(request, 'distributionSimulator/index.html')

def cadastroCaracteristica(request):
    caracteristica = Caracteristica()
    context = {}
    if request.method == 'POST':
        form = CaracteristicaForm(request.POST)
        if form.is_valid():
            context['is_valid'] = True
            caracteristica = form.save(form)
            messages.add_message(request, messages.SUCCESS, 'Característica salva com sucesso.')
            form = CaracteristicaForm()
            context['message'] = messages.get_messages(request)
    else:
        form = CaracteristicaForm()
        
    context['form'] = form
    context['caracteristica'] = caracteristica
    template_name = 'distributionSimulator/cadastroCaracteristica.html'
    
    return render(request, template_name, context)

def preencherCaracteristicaDto():
    caracteristicas = Caracteristica.objects.all()
    caracteristicasDto = []
    for caracteristica in caracteristicas:
        caracteristicaDto = CaracteristicaRecursoDto()
        caracteristicaDto.caracteristica = caracteristica
        caracteristicasDto.append(caracteristicaDto)
        
    return caracteristicasDto

def cadastroRecurso(request):
    recurso = Recurso()
    context = {}
    recursos = None
    if request.method == 'POST':
        form = RecursoForm(request.POST)
        if form.is_valid():
            context['is_valid'] = True
            id_recurso = form.saveRecurso(form)
            form.saveCaracteristicaRecurso(id_recurso)        
            messages.add_message(request, messages.SUCCESS, 'Recurso salvo com sucesso.')
            form = RecursoForm()
            caracteristicaDto = preencherCaracteristicaDto()
            context['message'] = messages.get_messages(request)
    else:
        form = RecursoForm()
        recursos = form.carregarTodosRecursos()
        caracteristicaDto = preencherCaracteristicaDto()
        
    context['form'] = form
    context['recurso'] = recurso
    context['recursos'] = recursos
    context['caracteristicaDto'] = caracteristicaDto
    template_name = 'distributionSimulator/cadastroRecurso.html'
    
    return render(request, template_name, context)

def cadastroTarefa(request):
    tarefa = Tarefa()
    context = {}
    if request.method == 'POST':
        form = TarefaForm(request.POST)
        if form.is_valid():
            context['is_valid'] = True
            id_tarefa = form.saveTarefa(form)
            form.saveCaracteristicaTarefa(id_tarefa)        
            messages.add_message(request, messages.SUCCESS, 'Tarefa salva com sucesso.')
            form = TarefaForm()
            caracteristicaDto = preencherCaracteristicaDto()
            context['message'] = messages.get_messages(request)
    else:
        form = TarefaForm()
        caracteristicaDto = preencherCaracteristicaDto()
        
    context['form'] = form
    context['tarefa'] = tarefa
    context['caracteristicaDto'] = caracteristicaDto
    template_name = 'distributionSimulator/cadastroTarefa.html'
    
    return render(request, template_name, context)

def preencherSimuladorRecursoDto():
    recursos = Recurso.objects.all()
    listaSimuladorRecursoDto = []
    for recurso in recursos:
        simuladorRecursoDto = SimuladorRecursoDto()
        caracteristicasRecurso = CaracteristicaRecurso.objects.pesquisarPorRecurso(recurso).values_list('id_caracteristica', 'nivel_conhecimento')
        listaCaracteristica = []
        listaCaracteristicaRecurso = []
        for caracteristicaRecurso in caracteristicasRecurso:
            cr = CaracteristicaRecurso()
            caracteristica = Caracteristica.objects.pesquisaPorId(caracteristicaRecurso[0]).first()
            cr.id_caracteristica = caracteristica
            cr.nivel_conhecimento = caracteristicaRecurso[1]
            listaCaracteristica.append(caracteristica)
            listaCaracteristicaRecurso.append(cr)
        simuladorRecursoDto.caracteristicas = listaCaracteristica
        simuladorRecursoDto.recurso = recurso
        simuladorRecursoDto.caracteristicaRecurso = listaCaracteristicaRecurso
        listaSimuladorRecursoDto.append(simuladorRecursoDto)
        
    return listaSimuladorRecursoDto

def preencherSimuladorTarefaDto():
    tarefas = Tarefa.objects.all()
    listaSimuladorTarefaDto = []
    for tarefa in tarefas:
        simuladorTarefaDto = SimuladorTarefaDto()
        caracteristicasTarefa = CaracteristicaTarefa.objects.pesquisarPorTarefa(tarefa).values_list('id_caracteristica', 'nivel_complexidade')
        listaCaracteristica = []
        listaCaracteristicasTarefa = []
        for caracteristicaTarefa in caracteristicasTarefa:
            ct = CaracteristicaTarefa()
            caracteristica = Caracteristica.objects.pesquisaPorId(caracteristicaTarefa[0]).first()
            ct.id_caracteristica = caracteristica
            ct.nivel_complexidade = caracteristicaTarefa[1]
            listaCaracteristica.append(caracteristica)
            listaCaracteristicasTarefa.append(ct)
        simuladorTarefaDto.caracteristicas = listaCaracteristica
        simuladorTarefaDto.tarefa = tarefa
        simuladorTarefaDto.caracteristicaTarefa = listaCaracteristicasTarefa
        listaSimuladorTarefaDto.append(simuladorTarefaDto)
        
    return listaSimuladorTarefaDto 

def simulador(request):
    context = {}
    simuladorEscolhido = None
    if request.method == 'POST':
        form = SimuladorForm(request.POST)
        if form.is_valid():
            context['is_valid'] = True
            simuladorEscolhido = form.simularAprendizado(form)      
            messages.add_message(request, messages.SUCCESS, 'Simulação salva com sucesso.')
            form = SimuladorForm()
            context['message'] = messages.get_messages(request)
    else:
        form = SimuladorForm()
        
    listaSimuladorRecursoDto = preencherSimuladorRecursoDto()
    listaSimuladorTarefaDto = preencherSimuladorTarefaDto()
    
    context['form'] = form
    context['listaSimuladorRecursoDto'] = listaSimuladorRecursoDto
    context['listaSimuladorTarefaDto'] = listaSimuladorTarefaDto
    context['simuladorEscolhido'] = simuladorEscolhido
    template_name = 'distributionSimulator/simulador.html'
    return render(request, template_name, context)


def simulador2(request):
    
    context={}
    simuladorEscolhido = None
    task = None
    tarefasDto = None
    
    if request.method == 'POST':
        
        form = SimuladorForm()
        qtdTime = request.POST.get('qtdMembros')
        botao = request.POST.get('aprendizado')
        
        if botao != 'aprendizado':
            botao = request.POST.get('tempo')
            if botao != 'tempo':
                botao = request.POST.get('novaSimulacao')
                if botao == 'novaSimulacao':
                    return redirect('/distributionSimulatorApp/simulador2')
        
        idTarefaEscolhida = None
        
        tarefas = form.todasTarefas()
        
        for tarefa in tarefas:
            checkTarefa = request.POST.get(str(tarefa.id_node))
            
            if checkTarefa != None:
                idTarefaEscolhida = checkTarefa
                break
        
        if (qtdTime != None and int(qtdTime) > 0):
            if botao != 'tempo':
                simuladorEscolhido = form.aprendizadoSimularNovo(qtdTime, idTarefaEscolhida)
                task = form.retornarTarefaPorId(idTarefaEscolhida)
                timeTeste = None
                timeTeste = form.verificarSeTarefaTemTimeSalvo(idTarefaEscolhida, 'aprendizado')
                if timeTeste == None or not timeTeste:
                    for simulador in simuladorEscolhido:
                        time = Time()
                        time.id_recurso = simulador.recurso
                        time.id_node = task
                        time.tipo = 'aprendizado'
                        form.salvarTime(time)
            else:
                simuladorEscolhido = form.tempoSimularNovo(qtdTime, idTarefaEscolhida)
                task = form.retornarTarefaPorId(idTarefaEscolhida)
                timeTeste = None
                timeTeste = form.verificarSeTarefaTemTimeSalvo(idTarefaEscolhida, 'tempo')
                if timeTeste == None or not timeTeste:
                    for simulador in simuladorEscolhido:
                        time = Time()
                        time.id_recurso = simulador.recurso
                        time.id_node = task
                        time.tipo = 'tempo'
                        form.salvarTime(time)
    else:
        form = SimuladorForm()
        form.tratarTarefasVencidas()
        tarefasDto = form.preencherTarefaDto()
    
    context['tarefasDto'] = tarefasDto
    context['simuladorEscolhido'] = simuladorEscolhido
    context['form'] = form
    context['task'] = task
    template_name = 'distributionSimulator/simulador2.html'
    
    return render(request,template_name,context)

def diagrama(request):
    context={}
    if request.method == 'POST':
        form = DiagramaForm(request.POST, request.FILES)
        if form.is_valid():
            diagramaFile = form.cleaned_data['diagrama_file']
            form.salvarDiagrama(diagramaFile)  
    else:
        form = DiagramaForm();
        
    context['form'] = form
    template_name = 'distributionSimulator/uploadJsonDiagram.html'
    return render(request, template_name, context)


       
def cadastroTarefa2(request):
    context = {}
    if request.method == 'GET':
        idTarefa = request.GET.get('id')
        print(idTarefa)
        if idTarefa == None:
            form = Tarefa2Form()
            tarefas = NodeCGM.objects.retornarTodasTarefas()
            
    context['form'] = form
    context['tarefas'] = tarefas
    template_name = 'distributionSimulator/cadastroTarefa2.html'
    
    return render(request, template_name, context)

def preencherCaracteristicaTarefaDto(idTarefa):
    caracteristicas = Caracteristica.objects.pesquisarTodas()
    caracteristicasTarefaDto = []
    
    for caracteristica2 in caracteristicas:
        
        caracteristicaTarefaDto = CaracteristicaTarefaDto()
        caracteristicaTarefaDto.caracteristica = caracteristica2
        caracteristicaTarefaDto.nivelConhecimento = ""
        caracteristicaTarefaDto.id_tarefa = idTarefa
        
        caracteristicasTarefaDto.append(caracteristicaTarefaDto)
        
    return caracteristicasTarefaDto
    
def caracterizarTarefa(request):
    context={}
    
    dtInicio = ""
    dtFim = ""
    descricao = ""
    
    if request.method == 'POST':
        idTarefa = request.GET.get('id')
        form = CaracteristicaTarefa2Form()
        
        if idTarefa != None:
            caracteristicasTarefaDto = form.preencherCaracteristicaNodeCGM(idTarefa)
            cns = []
            dtInicio = request.POST.get('dtInicio')
            dtFim = request.POST.get('dtFim')
            descricao = request.POST.get('descricao')
            
            for caracteristicaTarefaDto in caracteristicasTarefaDto:
                nivelComplexidade = request.POST.get(str(caracteristicaTarefaDto.id_caracteristica.id_caracteristica))
                if nivelComplexidade != None and nivelComplexidade != "" and nivelComplexidade != str():
                    caracteristicaTarefaDto.nivel_complexidade = nivelComplexidade
                    cns.append(caracteristicaTarefaDto)
            
            form.atualizarTarefa(idTarefa, dtInicio, dtFim,descricao)
            form.excluirCaracteristicaCGMPorNode(idTarefa)
            form.salvarCaracteristicasTarefa(cns)
            
    else:
        idTarefa = request.GET.get('id')
        form = CaracteristicaTarefa2Form()
        if idTarefa != None:
            tarefa = form.pesquisarNodeCGMPorId(idTarefa)
            if tarefa.dt_inicio != None and tarefa.dt_inicio != "":
                if str(tarefa.dt_inicio).__contains__('/'):
                    dtInicio = datetime.strptime(str(tarefa.dt_inicio), '%d/%m/%Y')
                    dtInicio = dtInicio.strftime('%Y-%m-%d')
                else:
                    dtInicio = str(tarefa.dt_inicio)
            if tarefa.dt_fim != None and tarefa.dt_fim != "":
                if str(tarefa.dt_fim).__contains__('/'):
                    dtFim = datetime.strptime(str(tarefa.dt_fim), '%d/%m/%Y')
                    dtFim = dtFim.strftime('%Y-%m-%d')
                else:
                    dtFim = str(tarefa.dt_fim)
            descricao = tarefa.descricao
            caracteristicasTarefaDto = form.preencherCaracteristicaNodeCGM(idTarefa)
    
    context['form'] = form
    context['caracteristicasTarefaDto'] = caracteristicasTarefaDto
    context['dtInicio'] = dtInicio
    context['dtFim'] = dtFim
    context['descricao'] = descricao
    template_name = 'distributionSimulator/caracterizarTarefa.html'
    
    return render(request, template_name, context)

def feedbackParte1(request):    
    context={}
    template_name = ""
    tarefas = None
    
    if request.method == 'POST':
        form = FeedBackForm()
    else:
        form = FeedBackForm()
        template_name = 'distributionSimulator/feedback.html'
        tarefas = form.carregarTarefasFeedBack()
        context['form'] = Form
        context['tarefas'] = tarefas
        
        return render(request, template_name, context)
    
def feedBackParte2(request):
    context={}
    template_name = ""
    idTarefa = None
    tarefa = None
    timeTempo = None
    timeAprendizado = None
    form = None
    caracteristicasTarefa = None
    
    if request.method == 'POST':
        form = FeedBackForm()
        edicao = request.POST.get('editarTarefa')
        caracteristica = request.POST.get('caracteristica')
        excluirTime = request.POST.get('excluirTime')
        excluirTimeA = request.POST.get('excluirTimeA')
        
        if edicao != None:
            return redirect('/distributionSimulatorApp/edicaoTarefa?id=' + str(edicao))
        else:
            if caracteristica != None:
                caracteristicasTarefa = form.carregarCaracteristicasTarefa(caracteristica)
                cts = []
                for ct in caracteristicasTarefa:
                    nivelComplexidade = request.POST.get(str(ct.id_caracteristica.id_caracteristica))
                    
                    if nivelComplexidade != None and nivelComplexidade != "" and nivelComplexidade != str():
                        ct.nivel_complexidade = nivelComplexidade
                        cts.append(ct)
                        
                if cts != None:
                    form.atualizarCaracteristicasTarefa(cts)
                return redirect('/distributionSimulatorApp/feedbackParte1')
            else:
                if excluirTime != None:
                    form.excluirTime(excluirTime, 'tempo')
                    return redirect('/distributionSimulatorApp/feedbackParte2?id='+excluirTime)
                else:
                    if excluirTimeA != None:
                        form.excluirTime(excluirTimeA, 'aprendizado')
                        return redirect('/distributionSimulatorApp/feedbackParte2?id='+excluirTimeA)
                    else:
                        return redirect('/distributionSimulatorApp/feedbackParte1')
    else:
        form = FeedBackForm()
        idTarefa = request.GET.get("id")
        
        if idTarefa != None:
            tarefa = form.carregarTarefaPorId(idTarefa)
            timeTempo = form.carregarTimePorTipo(idTarefa, 'tempo')
            timeAprendizado = form.carregarTimePorTipo(idTarefa, 'aprendizado')
            caracteristicasTarefa = form.carregarCaracteristicasTarefa(idTarefa)
            context['form'] = form 
            context['tarefa'] = tarefa
            context['timeTempo'] = timeTempo
            context['timeAprendizado'] = timeAprendizado
            context['caracteristicasTarefa'] = caracteristicasTarefa
            template_name = 'distributionSimulator/detalhesTarefa.html'
            
            return render(request, template_name, context)
      
def edicaoTarefa(request):
    context={}
    tarefa=None
    idTarefa = None
    dtInicio = None
    dtFim = None
    descricao = None
    form = None
    template_name = ""
    
    if request.method == 'POST':
        form = FeedBackForm()
        idTarefa = request.POST.get('editarTarefa')
        descricao = request.POST.get('descricao')
        dtInicio = request.POST.get('dtInicio')
        dtFim = request.POST.get('dtFim')
        
        tarefa = form.carregarTarefaPorId(idTarefa)
        tarefa.descricao = descricao
        tarefa.dt_inicio = dtInicio
        dtFim = datetime.strptime(dtFim,'%Y-%m-%d')
        tarefa.dt_fim = dtFim
        if tarefa.dt_fim > datetime.now():
            tarefa.tarefaRealizada = False
        form.atualizarTarefa(tarefa)
        return redirect('/distributionSimulatorApp/feedbackParte2?id='+ str(tarefa.id_node))
    
    else:
        idTarefa = request.GET.get('id')
        form = FeedBackForm()
        tarefa = form.carregarTarefaPorId(idTarefa)
        dtInicio = tarefa.dt_inicio
        if str(dtInicio).__contains__('/'):
            dtInicio = datetime.strptime(str(dtInicio), '%d/%m/%Y')
            dtInicio = dtInicio.strftime('%Y-%m-%d')
        dtFim = tarefa.dt_fim
        if str(dtFim).__contains__('/'):
            dtFim = datetime.strptime(str(dtFim),'%d/%m/%Y')
            dtFim = dtFim.strftime('%Y-%m-%d')
        descricao = tarefa.descricao
        
        context['dtInicio'] = dtInicio
        context['dtFim'] = dtFim
        context['descricao'] = descricao
        context['tarefa'] = tarefa
        template_name = 'distributionSimulator/editarTarefa.html'
        
        return render(request, template_name, context)
    
def edicaoRecurso(request):
    context={}
    idRecurso=None
    caracteristicasRecursoNovo = []
    nome = None
    matricula = None
    template_name = ""
    form = None
    
    if request.method == 'POST':
        form = FeedBackForm()
        nome = request.POST.get('nome')
        matricula = request.POST.get('matricula')
        editarRecurso = request.POST.get('editarRecurso')
        caracteristicasRecurso = form.preencherCaracteristicasRecurso(editarRecurso)
        crs = []
        
        recurso = form.carregarRecurso(editarRecurso)
        recurso.nome_recurso = nome 
        recurso.matricula_recurso = matricula
        
        for cr in caracteristicasRecurso:
            nivelConhecimento = request.POST.get(str(cr.id_caracteristica.id_caracteristica))
            
            if nivelConhecimento != None and nivelConhecimento != "" and nivelConhecimento != str(): 
                cr.nivel_conhecimento = nivelConhecimento
                crs.append(cr)
                
        form.excluirTodasCaracteristicasRecurso(editarRecurso)
        form.salvarCaracteristicasRecurso(crs)
        form.atualizarRecurso(recurso)
        
        return redirect('/distributionSimulatorApp/feedbackParte1')
    
    else:
        form = FeedBackForm()
        idRecurso = request.GET.get('id')
        recurso = form.carregarRecurso(idRecurso)
        nome = recurso.nome_recurso
        matricula = recurso.matricula_recurso
        caracteristicasRecursoNovo = form.preencherCaracteristicasRecurso(idRecurso)
        
        context['recurso'] = recurso
        context['nome'] = nome 
        context['matricula'] = matricula
        context['caracteristicasRecursoNovo'] = caracteristicasRecursoNovo
        template_name = 'distributionSimulator/editarRecurso.html'
        
        return render(request, template_name, context)
        