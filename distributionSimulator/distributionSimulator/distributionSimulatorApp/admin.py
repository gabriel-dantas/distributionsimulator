from django.contrib import admin

# Register your models here.
from .models import Recurso
from .models import Caracteristica
from .models import CaracteristicaRecurso
from .models import CaracteristicaTarefa
from .models import NodeCGM
from .models import Dependencia
from .models import RecursoTarefa
from .models import Tarefa
from .models import TipoRelacao
from .models import Usuario

admin.site.register(Recurso)
admin.site.register(Caracteristica)
admin.site.register(CaracteristicaRecurso)
admin.site.register(CaracteristicaTarefa)
admin.site.register(NodeCGM)
admin.site.register(Dependencia)
admin.site.register(RecursoTarefa)
admin.site.register(Tarefa)
admin.site.register(TipoRelacao)
admin.site.register(Usuario)