'''
Created on 8 de out de 2017

@author: gabriel
'''
from django import forms
from .models import Caracteristica
from .models import Recurso
from .models import CaracteristicaRecurso
from .models import Tarefa
from .models import CaracteristicaTarefa
from .models import DiagramaJsonFile
from .simuladorCore import Simulador
from django.utils import timezone
import json
import operator
from datetime import datetime
from .models import Contexto
from .models import NodeCGM,Dependencia,Time
from .models import CaracteristicaNodeCGM
from .dto import SimuladorTarefaDto2
from . import decisionTree
from .dto import SimuladorTimeEscolhidos



class CaracteristicaForm(forms.Form):
    descricao = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Característica'})) 
    
    def save(self,commit=True):
        caracteristica = Caracteristica()
        caracteristica.descricao = self.cleaned_data['descricao']
        if commit:
            caracteristica._get_pk_val(None)
            caracteristica.save()
        return caracteristica
        

class RecursoForm(forms.Form):
    nome = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nome'}))
    matricula = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Matrícula'}))
    selecionados = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Características Selecionadas Separadas por vírgula'}))
    nivelConhecimento = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nível de Conhecimento a característica em ordem Separadas por vírgula'}))
    
    
    def saveRecurso(self, commit=True):
        recurso = Recurso()
        recurso.nome_recurso = self.cleaned_data['nome']
        recurso.matricula_recurso = self.cleaned_data['matricula']
        if commit:
            recurso._get_pk_val(None)
            recurso.save()
        return recurso.id_recurso
    
    def gerarCaracteristicasRecurso(self, id_recurso):
        selecionadosTela = self.cleaned_data['selecionados']
        nivelConhecimentoTela = self.cleaned_data['nivelConhecimento']
        arrayIdsCaracteristicas = selecionadosTela.split(',')
        arrayNivelConhecimento = nivelConhecimentoTela.split(',')
        recursoAqui = Recurso.objects.pesquisaPorId(id_recurso).first()
        
        caracteristicasRecurso = []
        for i in range(0, arrayIdsCaracteristicas.__len__()):
            if(arrayIdsCaracteristicas[i].isdigit() and arrayNivelConhecimento[i].isdigit()):
                caracteristicaRecurso = CaracteristicaRecurso()
                caracteristica = Caracteristica.objects.pesquisaPorId(arrayIdsCaracteristicas[i]).first()
                caracteristicaRecurso.id_caracteristica = caracteristica
                caracteristicaRecurso.nivel_conhecimento = arrayNivelConhecimento[i]
                caracteristicaRecurso.id_recurso = recursoAqui
                caracteristicasRecurso.append(caracteristicaRecurso)
            
        return caracteristicasRecurso
        
    
    def saveCaracteristicaRecurso(self,id_recurso, commit=True):
        if commit:
            caracteristicasRecurso = RecursoForm.gerarCaracteristicasRecurso(self, id_recurso)
            
            for caracteristicaRecurso in caracteristicasRecurso:
                caracteristicaRecurso._get_pk_val(None)
                caracteristicaRecurso.save()
                
    def carregarTodosRecursos(self):
        return Recurso.objects.retornarTodosRecursos()
        
class TarefaForm(forms.Form):
    descricao = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Descrição da Tarefa'}))
    selecionados = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Características Selecionadas Separadas por vírgula'}))
    nivelComplexidade = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nível de Complexidade a característica em ordem Separadas por vírgula'}))
   
    def saveTarefa(self, commit=True):
        tarefa = Tarefa()
        tarefa.descricao = self.cleaned_data['descricao']
        if(commit):
            tarefa._get_pk_val(None)
            tarefa.save()
        
        return tarefa.id_tarefa
    
    def gerarCaracteristicasTarefa(self, id_tarefa):
        selecionadosTela = self.cleaned_data['selecionados']
        nivelComplexidadeTela = self.cleaned_data['nivelComplexidade']
        arrayIdsCaracteristicas = selecionadosTela.split(',')
        arrayNivelComplexidade = nivelComplexidadeTela.split(',')
        tarefa = Tarefa.objects.pesquisaPorId(id_tarefa).first()
        
        caracteristicasTarefa = []
        for i in range(0, arrayIdsCaracteristicas.__len__()):
            if(arrayIdsCaracteristicas[i].isdigit() and arrayNivelComplexidade[i].isdigit()):
                caracteristicaTarefa = CaracteristicaTarefa()
                caracteristica = Caracteristica.objects.pesquisaPorId(arrayIdsCaracteristicas[i]).first()
                caracteristicaTarefa.id_caracteristica = caracteristica
                caracteristicaTarefa.nivel_complexidade = arrayNivelComplexidade[i]
                caracteristicaTarefa.id_tarefa = tarefa
                caracteristicasTarefa.append(caracteristicaTarefa)
            
        return caracteristicasTarefa
    
    def saveCaracteristicaTarefa(self,id_tarefa,commit=True):
        if commit:
            caracteristicasTarefa = TarefaForm.gerarCaracteristicasTarefa(self,id_tarefa)
            for caracteristicaTarefa in caracteristicasTarefa:
                caracteristicaTarefa._get_pk_val(None)
                caracteristicaTarefa.save()
    
    
class SimuladorForm(forms.Form):
    tarefaEscolhida = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'id da tarefa escolhida para simulação'}))
    numeroRecurso = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Tamanho do Time'}))
    
    def simularAprendizado(self,commit=True):
        idTarefa = self.cleaned_data['tarefaEscolhida']
        numeroTime = self.cleaned_data['numeroRecurso']
        return Simulador.simuladorAprendizado(Simulador,numeroTime, idTarefa)
    
    def aprendizadoSimularNovo(self, qtdTime, idTarefa):
        simuladorEscolhido = []
        recursos = Recurso.objects.all()
        
        if recursos.__len__() >= int(qtdTime):
            time, pontuacoes = decisionTree.learningSimulation(qtdTime,idTarefa)
            recursosEscolhidos = Recurso.objects.pesquisaPorListaId(time)
            t = max(pontuacoes.items(), key=operator.itemgetter(1))[0]
            
            for recursoEscolhido in recursosEscolhidos:
                escolhido = SimuladorTimeEscolhidos()
                escolhido.recurso = recursoEscolhido
                if recursoEscolhido.id_recurso == t:
                    escolhido.lider = True
                escolhido.pontuacao = pontuacoes[recursoEscolhido.id_recurso]
                simuladorEscolhido.append(escolhido)
        
        simuladorEscolhido = sorted(simuladorEscolhido, key=lambda SimuladorTimeEscolhidos: SimuladorTimeEscolhidos.pontuacao, reverse=True)
        
        return simuladorEscolhido
    
    def tempoSimularNovo(self, qtdTime, idTarefa):
        simuladorEscolhido = []
        recursos = Recurso.objects.all()
        
        if recursos.__len__() >= int(qtdTime):
            time, pontuacoes = decisionTree.timeSimulation(qtdTime,idTarefa)
            recursosEscolhidos = Recurso.objects.pesquisaPorListaId(time)
            t = max(pontuacoes.items(), key=operator.itemgetter(1))[0]
            
            for recursoEscolhido in recursosEscolhidos:
                escolhido = SimuladorTimeEscolhidos()
                escolhido.recurso = recursoEscolhido
                if recursoEscolhido.id_recurso == t:
                    escolhido.lider = True
                escolhido.pontuacao = pontuacoes[recursoEscolhido.id_recurso]
                simuladorEscolhido.append(escolhido)
        
        simuladorEscolhido = sorted(simuladorEscolhido, key=lambda SimuladorTimeEscolhidos: SimuladorTimeEscolhidos.pontuacao, reverse=True)
        
        return simuladorEscolhido
    
    def tratarTarefasVencidas(self):
        tarefasVencidas = NodeCGM.objects.retornarTarefasVencidas()
        
        if tarefasVencidas != None:
            for tarefa in tarefasVencidas:
                tarefa.tarefaRealizada = True
                NodeCGM.objects.editarNodeCGM(tarefa)
            
    
    def retornarTarefasSimulacao(self):
        return NodeCGM.objects.retornarTarefasSimulacao()
    
    
    def todasTarefas(self):
        return NodeCGM.objects.retornarTodasTarefas()
    
    def retornarTarefaPorId(self,idTarefa):
        return NodeCGM.objects.pesquisarPorId(idTarefa)
    
    def preencherTarefaDto(self):
        tarefasDto = []
        tarefas = self.retornarTarefasSimulacao()
        
        for tarefa in tarefas:
            simuladorDto = SimuladorTarefaDto2()
            caracteristicasTarefa = CaracteristicaNodeCGM.objects.pesquisarCaracteristicaNodeTarefaPorTarefa(tarefa.id_node)
            simuladorDto.tarefa = tarefa
            simuladorDto.caracteristicaTarefa = caracteristicasTarefa
            tarefasDto.append(simuladorDto)
            
        return tarefasDto
    
    def salvarTime(self,time):
        time.save()
        
    def verificarSeTarefaTemTimeSalvo(self,idTarefa,tipo):
        return Time.objects.retornarTimePorTarefa(idTarefa,tipo)
    

class DiagramaForm(forms.ModelForm):
    
    def catchDate(self):
        data = timezone.now()
        return data
    
    def salvarDiagrama(self, diagramaFile,commit=True):
        caminhoFile = '/Users/gabriel/Documents/2:2017/TCC/git/distributionSimulator/distributionSimulator/distributionSimulatorApp/static/files/'
        diagramaJson = DiagramaJsonFile()
        diagramaJson.data_publicacao = self.catchDate()
        diagramaJson.diagrama_file = diagramaFile
        if commit:
            diagramaJson.save()
        caminhoFile = str(caminhoFile) + str(diagramaJson.diagrama_file) 
        with open(caminhoFile) as fileJson:
            dadosJson = json.load(fileJson)
            
            contadorActor = dadosJson['actors'].__len__()
            j = 0
            while (contadorActor > 0):
                contexto = Contexto()
                print(dadosJson['actors'][j]['text'])
                contexto.descricao = dadosJson['actors'][j]['text']
                contexto.id_diagrama = diagramaJson
                contexto.save()
                
                
                contador = dadosJson["actors"][j]["nodes"].__len__()
                i = 0
                while (contador > 0):
                    nodeCGM = NodeCGM()
                    nodeCGM.id_diagrama = diagramaJson
                    nodeCGM.id_contexto = contexto
                    nodeCGM.descricao = dadosJson["actors"][j]["nodes"][i]["text"]
                    nodeCGM.id_json_pistar = dadosJson["actors"][j]["nodes"][i]["id"]
                    if dadosJson["actors"][j]["nodes"][i]["type"].__contains__("Goal"):
                        nodeCGM.tipo = 'objetivo'
                    else:
                        nodeCGM.tipo = 'tarefa'
                    i = i + 1
                    contador= contador - 1
                    nodeCGM.save()
                
                contadorActor = contadorActor - 1
                j = j + 1
                
            contadorLink = dadosJson['links'].__len__()
            k = 0
            while (contadorLink > 0):
                dependencia = Dependencia()
                dependencia.id_diagrama = diagramaJson
                nodeCGMFonte = NodeCGM.objects.pesquisaPorIdJson(dadosJson["links"][k]["source"]).first()
                dependencia.fonte = nodeCGMFonte
                nodeCGMDestino = NodeCGM.objects.pesquisaPorIdJson(dadosJson["links"][k]["target"]).first()
                dependencia.destino = nodeCGMDestino
                dependencia.tipo = dadosJson["links"][k]["type"]
                k = k + 1
                contadorLink = contadorLink - 1
                dependencia.save()
            
    class Meta:
        model = DiagramaJsonFile
        fields = ('id_diagrama','diagrama_file')
        
    
class Tarefa2Form(forms.ModelForm):
    
    class Meta:
        model = NodeCGM
        fields = ('id_node', 'descricao')
        

class CaracteristicaTarefa2Form(forms.ModelForm):
    
    def salvarCaracteristicasTarefa(self,caracteristicasTarefaDto):
        
        if caracteristicasTarefaDto != None:
            for ccgm in  caracteristicasTarefaDto:
                ccgm.save()
    
    def pesquisarNodeCGMPorId(self,idTarefa):
        return NodeCGM.objects.pesquisarPorId(idTarefa)
                
    def atualizarTarefa(self,idTarefa,dtInicio,dtFim,descricao):
        tarefa = self.pesquisarNodeCGMPorId(idTarefa)
        tarefa.dt_inicio = dtInicio
        tarefa.dt_fim = dtFim
        tarefa.descricao = descricao
        NodeCGM.objects.editarNodeCGM(tarefa)
    
    def preencherCaracteristicaNodeCGM(self,idNodeCGM):
        
        nodeCGM = NodeCGM.objects.pesquisarPorId(idNodeCGM)
        caracteristicasNodeCGM = CaracteristicaNodeCGM.objects.pesquisarCaracteristicaNodeTarefaPorTarefa(idNodeCGM)
        caracteristicas = Caracteristica.objects.pesquisarTodas()
        cns = []
        
        for cn in caracteristicasNodeCGM:
            caracteristicas = caracteristicas.exclude(id_caracteristica = cn.id_caracteristica.id_caracteristica)
            cns.append(cn)
        
        for c in caracteristicas:
            cngm = CaracteristicaNodeCGM()
            cngm.descricao = 'Característica vinculada com sucesso'
            cngm.id_caracteristica = c 
            cngm.id_node = nodeCGM
            cngm.nivel_complexidade = ""
            cns.append(cngm)
            
        return cns
    
    def excluirCaracteristicaCGMPorNode(self,idNode):
        CaracteristicaNodeCGM.objects.excluirCaracteristicaNodeCGM(idNode)
        
    
    class Meta:
        model = CaracteristicaNodeCGM
        fields = ('id_caracteristica_tarefa', 'nivel_complexidade', 'id_node', 'id_caracteristica', 'descricao')
        
class FeedBackForm(forms.Form):
    
    def carregarTarefasFeedBack(self):
        tarefasFeedBack = NodeCGM.objects.retornarTarefasFeedBack()
        
        tarefasFeedBackRetorno = []
        
        for tf in tarefasFeedBack:
            tarefa = NodeCGM()
            tarefa.id_node = tf.id_node
            tarefa.descricao = tf.descricao
            dt_fim_formatado = datetime.strptime(str(tf.dt_fim),'%Y-%m-%d')
            tarefa.dt_fim = dt_fim_formatado.strftime('%d/%m/%Y')
            dt_inicio_formatado = datetime.strptime(str(tf.dt_inicio), '%Y-%m-%d')
            tarefa.dt_inicio = dt_inicio_formatado.strftime('%d/%m/%Y')
            tarefa.id_contexto = tf.id_contexto
            tarefa.id_diagrama = tf.id_diagrama
            tarefa.id_json_pistar = tf.id_json_pistar
            tarefa.tarefaRealizada = tf.tarefaRealizada
            tarefa.tipo = tf.tipo
            
            tarefasFeedBackRetorno.append(tarefa)
            
        return tarefasFeedBackRetorno
    
    def carregarTarefaPorId(self, idTarefa):
        
        tarefa = NodeCGM()
        tf = NodeCGM.objects.pesquisarPorId(idTarefa)
        
        tarefa.id_node = tf.id_node
        tarefa.descricao = tf.descricao
        dt_fim_formatado = datetime.strptime(str(tf.dt_fim),'%Y-%m-%d')
        tarefa.dt_fim = dt_fim_formatado.strftime('%d/%m/%Y')
        dt_inicio_formatado = datetime.strptime(str(tf.dt_inicio), '%Y-%m-%d')
        tarefa.dt_inicio = dt_inicio_formatado.strftime('%d/%m/%Y')
        tarefa.id_contexto = tf.id_contexto
        tarefa.id_diagrama = tf.id_diagrama
        tarefa.id_json_pistar = tf.id_json_pistar
        tarefa.tarefaRealizada = tf.tarefaRealizada
        tarefa.tipo = tf.tipo
        
        return tarefa
    
    def carregarTimePorTipo(self, idTarefa,tipo):
        return Time.objects.retornarTimePorTarefa(idTarefa, tipo)
    
    def carregarCaracteristicasTarefa(self,idTarefa):
        return CaracteristicaNodeCGM.objects.pesquisarCaracteristicaNodeTarefaPorTarefa(idTarefa)
            
    def atualizarCaracteristicasTarefa(self,caracteristicasTarefa):
        for ct in caracteristicasTarefa:
            CaracteristicaNodeCGM.objects.editarCaracteristicaNodeCGM(ct)
            
    def excluirTime(self,idNode,tipoTime):
        Time.objects.excluirTime(idNode, tipoTime)
        
    def atualizarTarefa(self,tarefa):
        NodeCGM.objects.editarNodeCGM(tarefa)
        
    def preencherCaracteristicasRecurso(self, idRecurso):
        caracteristicas = Caracteristica.objects.pesquisarTodas()
        recurso = Recurso.objects.pesquisaPorId(idRecurso).first()
        caracteristicasRecurso = CaracteristicaRecurso.objects.pesquisarPorRecurso(idRecurso)
        crs = []
        
        for crec in caracteristicasRecurso:
            caracteristicas = caracteristicas.exclude(id_caracteristica = crec.id_caracteristica.id_caracteristica)
            crs.append(crec)
        
        for c in caracteristicas:
            cr = CaracteristicaRecurso()
            cr.id_caracteristica = c
            cr.id_recurso = recurso
            cr.nivel_conhecimento = ""
            crs.append(cr)
            
        return crs
    
    def excluirTodasCaracteristicasRecurso(self,idRecurso):
        CaracteristicaRecurso.objects.excluir(idRecurso)
        
    def salvarCaracteristicasRecurso(self,caracteristicasRecurso):
        
        for cr in caracteristicasRecurso:
            cr.save()
            
    def atualizarRecurso(self,recurso):
        Recurso.objects.editarRecurso(recurso)
        
    def carregarRecurso(self,idRecurso):
        return Recurso.objects.pesquisaPorId(idRecurso).first()