'''
Created on 25 de out de 2017

@author: gabriel
'''
from .models import Caracteristica
from .models import Recurso
from .models import Tarefa
from .models import NodeCGM


class CaracteristicaRecursoDto():
    caracteristica = Caracteristica()
    nivelConhecimento = ""
    selecionado = False
    recurso = Recurso()
    
class CaracteristicaTarefaDto():
    caracteristica = Caracteristica()
    nivelConhecimento = ""
    id_tarefa = ""
    
class SimuladorRecursoDto():
    caracteristicas = []
    recurso = Recurso()
    caracteristicaRecurso = []
    
class SimuladorTarefaDto():
    caracteristicas = []
    tarefa = Tarefa()
    caracteristicaTarefa = []
    
class SimuladorTarefaDto2():
    caracteristicas = []
    tarefa = NodeCGM()
    caracteristicaTarefa = []
    
class SimuladorTimeEscolhidos():
    pontuacao = 0
    lider = False
    recurso = Recurso()
    
class RecursoPontuado():
    recurso = Recurso()
    pontuacao = 0;   
    
    def getKeyRecursosPontuados(self):
        return self.pontuacao 

    