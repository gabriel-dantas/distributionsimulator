# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-28 01:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Caracteristica',
            fields=[
                ('id_caracteristica', models.AutoField(primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='CaracteristicaRecurso',
            fields=[
                ('id_caracteristica_recurso', models.AutoField(primary_key=True, serialize=False)),
                ('id_caracteristica', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Caracteristica')),
            ],
        ),
        migrations.CreateModel(
            name='CaracteristicaTarefa',
            fields=[
                ('id_caracteristica_tarefa', models.AutoField(primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=255)),
                ('id_caracteristica', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Caracteristica')),
            ],
        ),
        migrations.CreateModel(
            name='Objetivo',
            fields=[
                ('id_objetivo', models.AutoField(primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ObjetivoTarefa',
            fields=[
                ('id_objetivo_tarefa', models.AutoField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Recurso',
            fields=[
                ('id_recurso', models.AutoField(primary_key=True, serialize=False)),
                ('nome_recurso', models.CharField(max_length=100)),
                ('matricula_recurso', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='RecursoTarefa',
            fields=[
                ('id_recurso_tarefa', models.AutoField(primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=255)),
                ('id_recurso', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Recurso')),
            ],
        ),
        migrations.CreateModel(
            name='Tarefa',
            fields=[
                ('id_tarefa', models.AutoField(primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='TipoRelacao',
            fields=[
                ('id_tipo_relacao', models.AutoField(primary_key=True, serialize=False)),
                ('descricao', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id_usuario', models.AutoField(primary_key=True, serialize=False)),
                ('nome', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=255)),
                ('senha', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='recursotarefa',
            name='id_tarefa',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Tarefa'),
        ),
        migrations.AddField(
            model_name='objetivotarefa',
            name='id_tipo_relacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.TipoRelacao'),
        ),
        migrations.AddField(
            model_name='caracteristicatarefa',
            name='id_tarefa',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Tarefa'),
        ),
        migrations.AddField(
            model_name='caracteristicarecurso',
            name='id_recurso',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Recurso'),
        ),
    ]
