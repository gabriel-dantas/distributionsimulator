# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-04-11 22:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('distributionSimulatorApp', '0006_auto_20180411_2237'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='caracteristicatarefa',
            name='id_nodeCGM',
        ),
        migrations.AddField(
            model_name='caracteristicatarefa',
            name='id_tarefa',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='distributionSimulatorApp.Tarefa'),
        ),
    ]
