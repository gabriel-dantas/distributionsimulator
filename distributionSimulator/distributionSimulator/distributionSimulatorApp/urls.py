
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^$', views.index,name='index'),
    url(r'^login/$', auth_views.login, {'template_name': 'distributionSimulator/login.html'}),
    url(r'^cadastroCaracteristica/$', views.cadastroCaracteristica,name='cadastroCaracteristica'),
    url(r'^cadastroRecurso/$', views.cadastroRecurso,name='cadastroRecurso'),
    url(r'^cadastroTarefa/$', views.cadastroTarefa,name='cadastroTarefa'),
    url(r'^simulador/$', views.simulador,name='simulador'),
    url(r'^simulador2/$', views.simulador2,name='simulador2'),
    url(r'^cadastroDiagrama/$', views.diagrama,name='diagrama'),
    url(r'^cadastro2Tarefa/$', views.cadastroTarefa2,name='cadastro2Tarefa'),
    url(r'^caracterizarTarefa/$', views.caracterizarTarefa,name='caracterizarTarefa'),
    url(r'^feedbackParte1/$', views.feedbackParte1,name='feedbackParte1'),
    url(r'^feedbackParte2/$', views.feedBackParte2,name='feedbackParte2'),
    url(r'^edicaoTarefa/$', views.edicaoTarefa,name='edicaoTarefa'),
    url(r'^edicaoRecurso/$', views.edicaoRecurso,name='edicaoRecurso')
]