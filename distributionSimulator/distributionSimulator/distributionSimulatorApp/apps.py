from django.apps import AppConfig


class DistributionsimulatorappConfig(AppConfig):
    name = 'distributionSimulatorApp'
