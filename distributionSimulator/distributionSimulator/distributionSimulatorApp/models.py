'''
Created on 27 de set de 2017

@author: gabriel
'''
from django.db import models
from time import time


class RecursoManager(models.Manager):
    def pesquisaPorId(self,query):
        return self.get_queryset().filter(
            id_recurso__exact = query
            )
        
    def pesquisarPorMatricula(self,query):
        return self.get_queryset().filter(
            matricula_recurso__icontains = query
            )
        
    def pesquisaPorListaId(self,query):
        return self.get_queryset().filter(
            pk__in = query
            )
        
    def salvar(self):
        return self.get_queryset().save()
    
    def editarRecurso(self,recurso):
        self.filter(
            id_recurso__exact = recurso.id_recurso
            ).update(
                id_recurso = recurso.id_recurso,
                nome_recurso = recurso.nome_recurso,
                matricula_recurso = recurso.matricula_recurso
                )
    
    def excluir(self,idRecurso):
        return self.filter(
            id_recurso__exact = idRecurso
            ).delete()
    
    def retornarTodosRecursos(self):
        return self.all()
    
'''Representa a pessoa que executará a tarefa'''
class Recurso (models.Model):
    id_recurso = models.AutoField(primary_key=True)
    nome_recurso = models.CharField('Nome',max_length = 100)
    matricula_recurso = models.CharField('Matrícula', max_length = 20)
    objects = RecursoManager()

'''Caracteristicas que serão compartilhadas entre o recurso
e a Tarefa pela qual escolheremos quem executará o que
'''
    
class CaracteristicaManager(models.Manager):
    def pesquisaPorId(self,query):
        return self.get_queryset().filter(
            id_caracteristica__exact = query
            )
        
    def pesquisarPorDescricao(self,query):
        return self.get_queryset().filter(
            descricao__icontains = query
            )
    def pesquisarTodas(self):
        return self.all()
        
    def salvar(self):
        return self.get_queryset().save()
    
    def excluir(self):
        return self.get_queryset().delete()

class Caracteristica (models.Model):
    id_caracteristica = models.AutoField(primary_key=True)
    descricao = models.CharField('Descrição Característica',max_length = 255)
    
    objects = CaracteristicaManager()

'''Faz o relacionamento entre caracterisca e recurso, demonstrando o nível
de conhecimento da parte envolvida nesta caracteristica especifica
'''
class CaracteristicaRecursoManager(models.Manager):
    def pesquisaPorId(self,query):
        return self.get_queryset().filter(
            id_caracteristica_recurso__exact = query
            )
        
    def pesquisarPorRecurso(self,query):
        return self.get_queryset().filter(
            id_recurso__exact = query
            )
    
    def salvar(self):
        return self.get_queryset().save()
    
    def excluir(self,query):
        return self.filter(
            id_recurso__exact = query
            ).delete()

class CaracteristicaRecurso(models.Model):
    id_caracteristica_recurso = models.AutoField(primary_key = True)
    id_recurso = models.ForeignKey(Recurso)
    id_caracteristica = models.ForeignKey(Caracteristica)
    nivel_conhecimento = models.IntegerField('Pontuação Conhecimento' , null = True)
    objects = CaracteristicaRecursoManager()
    

'''Parte executável de um objetivo que a soma de algumas tarefas
tornam um objetivo concluído
'''
class TarefaManager(models.Manager):
    def pesquisaPorId(self,query):
        return self.get_queryset().filter(
            id_tarefa__exact = query
            )
        
    def pesquisarPorDescricao(self,query):
        return self.get_queryset().filter(
            descricao__icontains = query
            )
        
    def salvar(self):
        return self.get_queryset().save()
    
    def excluir(self):
        return self.get_queryset().delete()
    
class Tarefa (models.Model):
    id_tarefa = models.AutoField(primary_key = True)
    descricao = models.CharField('Descrição Tarefa' ,max_length = 255)
    objects = TarefaManager()

'''Esclarece se para cumprir este objetivo o conjunto de tarefas tem que ser realizadas
or/and/xor
'''
class TipoRelacao (models.Model):
    id_tipo_relacao = models.AutoField(primary_key = True)
    descricao = models.CharField('Descrição Tipo de Relação' ,max_length = 255)
    


'''Caracteristicas da tarefa levando em conta o nivel de complexidade dado pelo
usuario
'''
class CaracteristicaTarefaManager(models.Manager):
    def pesquisaPorId(self,query):
        return self.get_queryset().filter(
            id_caracteristica_tarefa__exact = query
            )
        
    def pesquisarPorDescricao(self,query):
        return self.get_queryset().filter(
            descricao__icontains = query
            )
        
    def pesquisarPorTarefa(self,query):
        return self.get_queryset().filter(
            id_tarefa__exact = query
            )
    
    def salvar(self):
        return self.get_queryset().save()
    
    def excluir(self):
        return self.get_queryset().delete()
class CaracteristicaTarefa (models.Model):
    id_caracteristica_tarefa = models.AutoField(primary_key = True)
    nivel_complexidade = models.IntegerField('Pontuação Característica da Tarefa', null=True)
    id_tarefa = models.ForeignKey(Tarefa, null = True)
    id_caracteristica = models.ForeignKey(Caracteristica)
    descricao = models.CharField('Descrição do Relacionamento Característica Tarefa', max_length = 255)
    objects = CaracteristicaTarefaManager()

'''Apos a execução da tarefa pontuasse o recurso na execução de determinada tarefa
'''
class RecursoTarefa (models.Model):
    id_recurso_tarefa = models.AutoField(primary_key = True)
    id_recurso = models.ForeignKey(Recurso)
    id_tarefa = models.ForeignKey(Tarefa)
    data_conclusao = models.DateField('Data Conclusão Tarefa', null=True)
    nivel_qualidade = models.IntegerField('Nível de Qualidade Apresentado', null=True)
    descricao = models.CharField('Descrição do Relacionamento da Tarefa com o Recurso' , max_length = 255)

'''Usuários que utilizarão o sistema
'''
class Usuario (models.Model):
    id_usuario = models.AutoField(primary_key = True)
    nome = models.CharField('Nome',max_length = 100)
    email = models.CharField('Email',max_length = 255)
    senha = models.CharField('Senha',max_length = 255)
    

    
'''Aqui estarei criando uma classe que irá salvar a posicao onde o arquivo json do diagrama gerado será salvo
'''
def get_upload_file_name(instance, filename):
    return "%s_%s" % (str(time()).replace('.','_'), filename)

class DiagramaManager(models.Manager):
    def pesquisaPorId(self,query):
        return self.get_queryset().filter(
            id_diagrama__exact = query
            )

class DiagramaJsonFile(models.Model):
    id_diagrama = models.AutoField(primary_key = True)
    data_publicacao = models.DateTimeField('date published')
    diagrama_file = models.FileField(upload_to=get_upload_file_name)
    objects = DiagramaManager()
    
'''Contextos possiveis para simulacao
'''
class Contexto (models.Model):
    id_contexto = models.AutoField(primary_key = True)
    id_diagrama = models.ForeignKey(DiagramaJsonFile, null = True)
    descricao = models.CharField('Descricao',max_length = 255)

'''Objetivo, como estudado no GORE e como este projeto irá ser guiado para fins de tomada de
decisão
'''
class NodeCGMManager(models.Manager):
    def pesquisaPorIdJson(self,query):
        return self.get_queryset().filter(
            id_json_pistar__exact = query
            )
    
    def retornarTodasTarefas(self):
        return self.get_queryset().filter(
            tipo = 'tarefa'
            )
    
    def retornarTarefasVencidas(self):
        return self.raw("SELECT * from distributionSimulatorApp_nodecgm " +
            "where dt_fim <= date('now', 'localtime') and tarefaRealizada = 0")
    
    def retornarTarefasFeedBack(self):
        return self.raw("SELECT * from distributionSimulatorApp_nodecgm " +
                        "where tarefaRealizada = 1 and tipo = 'tarefa'")
        
    def retornarTarefasSimulacao(self):
        return self.get_queryset().filter(
            tarefaRealizada = False
            ).filter(
                tipo = 'tarefa'
                )
    
    def pesquisarPorId(self,query):
        return self.get_queryset().filter(
            id_node__exact = query
            ).first()
    
    def editarNodeCGM(self,nodeCGM):
        self.filter(
            id_node__exact = nodeCGM.id_node
            ).update(id_node = nodeCGM.id_node,tipo = nodeCGM.tipo,
                     id_contexto = nodeCGM.id_contexto, id_diagrama = nodeCGM.id_diagrama,
                     id_json_pistar = nodeCGM.id_json_pistar, dt_inicio = nodeCGM.dt_inicio,
                     dt_fim = nodeCGM.dt_fim, tarefaRealizada = nodeCGM.tarefaRealizada,
                     descricao = nodeCGM.descricao)
    
class NodeCGM (models.Model):
    id_node = models.AutoField(primary_key = True)
    tipo = models.CharField('Diz se é do tipo tarefa ou objetivo' ,max_length = 255)
    id_contexto = models.ForeignKey(Contexto, null = True)
    id_diagrama = models.ForeignKey(DiagramaJsonFile, null = True)
    id_json_pistar = models.CharField(max_length = 255, null = True)
    dt_inicio =  models.DateField(("Date"), null=True)
    dt_fim = models.DateField(("Date"), null = True)
    tarefaRealizada = models.BooleanField(default=False)
    descricao = models.CharField('Descrição Nó' ,max_length = 255)
    objects = NodeCGMManager()
    
'''Relacionamento entre tarefas e objetivos
'''

class Dependencia (models.Model):
    id_dependencia = models.AutoField(primary_key = True)
    fonte = models.ForeignKey(NodeCGM, related_name='fonte')
    destino = models.ForeignKey(NodeCGM, related_name='destino')
    tipo = models.CharField('Tipo de dependência' ,max_length = 255)
    id_diagrama = models.ForeignKey(DiagramaJsonFile, null = True)
   
class CaracteristicaNodeCGMManager(models.Manager):
    def pesquisarCaracteristicaNodeTarefaPorTarefa(self,query):
        return self.raw("SELECT cn.* "
                        +"from distributionSimulatorApp_caracteristicanodecgm as cn inner join " 
                        +"distributionSimulatorApp_nodecgm as nc on cn.id_node_id = nc.id_node "
                        +"where nc.tipo='tarefa' and cn.id_node_id = %s", [query])
        
    def pesquisarCaracteristicaNodeTodasTarefa(self):
        return self.raw("SELECT cn.* "
                        +"from distributionSimulatorApp_caracteristicanodecgm as cn inner join " 
                        +"distributionSimulatorApp_nodecgm as nc on cn.id_node_id = nc.id_node "
                        +"where nc.tipo='tarefa'")
        
    def editarCaracteristicaNodeCGM(self, caracteristicaTarefa):
        self.filter(
            id_caracteristica_tarefa__exact = caracteristicaTarefa.id_caracteristica_tarefa
            ).update(id_caracteristica_tarefa = caracteristicaTarefa.id_caracteristica_tarefa,
                     nivel_complexidade = caracteristicaTarefa.nivel_complexidade,
                     id_node = caracteristicaTarefa.id_node, id_caracteristica = caracteristicaTarefa.id_caracteristica,
                     descricao = caracteristicaTarefa.descricao)
            
    def excluirCaracteristicaNodeCGM(self,idNode):
        self.filter(
            id_node__exact=idNode
            ).delete()
 
'''Aqui sera criada a tabela de características dos nos, para nao mudar o que ja estava funcionando 
preferi nao fazer alteracoes bruscas
'''
    
class CaracteristicaNodeCGM (models.Model):
    id_caracteristica_tarefa = models.AutoField(primary_key = True)
    nivel_complexidade = models.IntegerField('Pontuação Característica da Tarefa', null=True)
    id_node = models.ForeignKey(NodeCGM, null = True)
    id_caracteristica = models.ForeignKey(Caracteristica)
    descricao = models.CharField('Descrição do Relacionamento Característica Tarefa', max_length = 255)
    objects = CaracteristicaNodeCGMManager()
    
class TimeManager(models.Manager):
    def retornarTimePorTarefa(self,idNode,tipoTime):
        return self.get_queryset().filter(
            id_node__exact = idNode
            ).filter(
                tipo = tipoTime
                )
            
    def excluirTime(self,idNode,tipoTime):
        self.filter(
            id_node__exact = idNode
            ).filter(
                tipo = tipoTime
                ).delete()
    
class Time(models.Model):
    id_time = models.AutoField(primary_key =  True)
    id_node = models.ForeignKey(NodeCGM, null = True)
    id_recurso = models.ForeignKey(Recurso, null = True)
    tipo = models.CharField('Tipo de contexto de simulacao', max_length=255, null=True)
    objects  = TimeManager()
    